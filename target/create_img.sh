
# Creates an empty disk image of 40 MB, creates its MBR

dd if=/dev/zero of=disk.img bs=1M count=40

# Partition of 32 MB

(echo o; echo n; echo p; echo; echo; echo +32M; echo t; echo e;  echo; echo a; echo w) | sudo fdisk disk.img

mkdir -p tmp

sudo kpartx -avs disk.img

sudo mkfs.vfat /dev/mapper/loop0p1

sudo kpartx -dvs disk.img

sudo kpartx -avs disk.img

sudo mount /dev/mapper/loop0p1 tmp

sudo cp merida-1.0.itb tmp

sudo umount tmp

sudo kpartx -dvs disk.img

parted disk.img print

#boot0 code

dd if=../u-boot-2016.09/sunxi_spl/boot0/boot0_sdcard.bin of=disk.img bs=1k seek=8 conv=notrunc

## Blocks for elmgr 

dd if=../sun50i-elmgr/build/elmgr.bin of=disk.img bs=512 seek=70000 conv=notrunc

## Blocks for U-boot code 

dd if=../u-boot-2016.09/u-boot-dtb.bin of=disk.img bs=512 seek=70100 conv=notrunc

