/*
 * U-boot its file definition for the REPTAR board
 * (c) 2017 REDS Institute, HEIG-VD
 */

/dts-v1/;

/ {
	description = "Simple image containing Reptar kernel and FDT blob";
	#address-cells = <0>;
	#size-cells = <0>;

	images {
	#address-cells = <0>;
		#size-cells = <0>;
		kernel {
			description = "REPTAR board kernel";
			data = /incbin/("../linux/arch/arm/boot/Image");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "none";
			load = <0x80008000>;
			entry = <0x80008000>;
			#address-cells = <0>;
			#size-cells = <0>;

			hash {
				algo = "crc32";
			};

		};
		ramdisk {
			description = "REDS minimal rootfs";
			data = /incbin/("../filesystems/rootfs.cpio");
			type = "ramdisk";
			arch = "arm";
			os = "linux";
			compression = "none";
			load = <0x84000000>;
			entry = <0x84000000>;

			hash {
				algo = "crc32";
			};
		};
		fdt {
			description = "Flattened Device Tree blob";
			data = /incbin/("../linux/arch/arm/boot/dts/omap3-reptar.dtb");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
			hash {
				algo = "crc32";
			};
		};
	};

	configurations {
		default = "conf";
		conf {
			description = "Boot REPTAR kernel with FDT blob & rootfs";
			kernel = "kernel";
			ramdisk = "ramdisk";
			fdt = "fdt";
		};
	};
};
