/*
 * timer.h
 *
 *  Created on: Oct 17, 2018
 *      Author: reds
 */

#define	PERIODIC_TIMER_BASE		0x1c110000
#define	ONESHOT_TIMER_BASE		0x1c120000
#define	TIMER_MAPPING_SIZE		0x1000
