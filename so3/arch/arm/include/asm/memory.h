/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 * 
 * This software is released under the MIT License whose terms are defined hereafter.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef ASM_MEMORY_H
#define ASM_MEMORY_H

#include <generated/autoconf.h>

#ifdef CONFIG_SO3VIRT

/*
 * Normally, VMALLOC_END (Linux) finishes at 0xf8000000, and some I/O like UARTs might be mapped.
 * So we preserve re-adjusting pfns in these regions (below the hypervisor).
 */
#define POST_MIGRATION_REMAPPING_MAX	0xf8000000
#define	HYPERVISOR_VIRT_ADDR		0xff000000
#define HYPERVISOR_VIRT_ADDR_DBG	0xf8000000
#define HYPERVISOR_VIRT_SIZE		0x00c00000

#define HYPERVISOR_VBSTORE_VADDR	0xffe01000

#ifdef __ASSEMBLY__
.extern avz_guest_phys_offset
#else
#include <types.h>
#include <list.h>
extern uint32_t avz_guest_phys_offset;

#endif /* __ASSEMBLY__ */

#define CONFIG_RAM_BASE		(avz_guest_phys_offset)

#endif /* CONFIG_SO3VIRT */
#ifndef __ASSEMBLY__
#include <types.h>

extern uint32_t *__sys_l1pgtable;

#endif /* __ASSEMBLY__ */

#endif /* ASM_MEMORY_H */
