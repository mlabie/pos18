@ SO3 Kernel entry point

#include <linkage.h>

#include <generated/autoconf.h>

#include <asm/processor.h>
#include <asm/mmu.h>
#include <asm/memory.h>
#include <thread.h>

.global _start
.global _fdt_addr

.extern clear_bss
.extern configure_l1pgtable
.extern cpu_setup
.extern __stack_base
.extern __vectors_start

#ifdef CONFIG_SO3VIRT

.global avz_guest_phys_offset
.extern avz_start_info

#endif /* CONFIG_SO3VIRT */

.section ".head.text","ax"

#if 0

/*
 * Helper macro to enter SVC mode cleanly and mask interrupts. reg is
 * a scratch register for the macro to overwrite.
 *
 * This macro is intended for forcing the CPU into SVC mode at boot time.
 * you cannot return to the original mode.
 */
.macro safe_svcmode_maskall reg:req

	mrs	\reg , cpsr
	eor	\reg, \reg, #HYP_MODE
	tst	\reg, #MODE_MASK
	bic	\reg , \reg , #MODE_MASK
	orr	\reg , \reg , #PSR_I_BIT | PSR_F_BIT | SVC_MODE

	bne	1f
	orr	\reg, \reg, #PSR_A_BIT
	adr	lr, 2f
	msr	spsr_cxsf, \reg

	__MSR_ELR_HYP(14)
	__ERET
1:	msr	cpsr_c, \reg
2:

.endm

#endif

_start:

  @ r1 = machine id
  @ r2 = dtb address

  @ Make sure we start in SVC mode

  msr  	cpsr_c, #PSR_F_BIT | PSR_I_BIT | SVC_MODE @ ensure svc mode and irqs disabled
  @safe_svcmode_maskall r9


#ifdef CONFIG_SO3VIRT

  ldr 	r10, start_info
  str 	r12, [r10]

#else /* !CONFIG_SO3VIRT */

  @ Set vector table at address ___vectors_start
  ldr 	r1, .LCvectors
  mcr 	p15, 0, r1, c12, c0, 0

#endif /* CONFIG_SO3VIRT */

  @ Preserve the (physical address of) device tree base in r9
  mov 	r9, r2

  @ Initialize stack pointers for current mode (normal case if no MMU is used)
  ldr  	sp, .LCstack_base
  add	sp, sp, #THREAD_STACK_SIZE

  @ Up to here, a stack should be initialized


__kernel_main:

  ldr	r0, =_fdt_addr
  str	r9, [r0]

  @ C main entry point
  b 	kernel_start

  @ never returns...

.ltorg

_fdt_addr:
  .word 	0

.align	2
__v7_setup_stack:
	.space	4 * 11				@ 11 registers

.align 8

@ Before MMU is enabled, we cannot refer to the normal stack as declared in the linker script
temp_stack_bottom:
	.space 1024
temp_stack:

#ifdef CONFIG_SO3VIRT

start_info:
    .long   avz_start_info

#endif /* CONFIG_SO3VIRT */

.LCvirt_entry:
  .word __kernel_main

.LCstack_base:
  .word __stack_base

.LCvectors:
  .word __vectors_start
