/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 * 
 * This software is released under the MIT License whose terms are defined hereafter.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - 2014-2017: Daniel Rossier
 */

#if 0
#define DEBUG
#endif

#include <compiler.h>
#include <schedule.h>
#include <process.h>
#include <thread.h>
#include <heap.h>
#include <softirq.h>
#include <mutex.h>
#include <timer.h>

#include <device/irq.h>

#include <asm/mmu.h>

static struct list_head readyThreads;
static struct list_head zombieThreads;

/* Global list of process */
struct list_head proc_list;
struct tcb *tcb_idle;

static tcb_t *current_thread;

static spinlock_t schedule_lock;

u64 jiffies = 0ull;
u64 jiffies_ref = 0ull;

static timer_t schedule_timer;

/*
 * The following code (normally disabled) is used for debugging purposes...
 */
#if 0
inline bool check_consistency_ready(void) {
	queue_thread_t *cur;
	tcb_t *_tcb;
	struct list_head *pos;

	list_for_each(pos, &readyThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);
		_tcb = cur->tcb;
		if (_tcb->state != THREAD_STATE_READY) {
			printk("### ERROR %d on tid %d with state: %s ###\n", __LINE__, _tcb->tid, print_state(_tcb));
			return false;
		}
	}
	return true;
}
#endif /* 0 */

tcb_t *current(void) {
	return current_thread;
}

inline void set_current(struct tcb *tcb) {
	current_thread = tcb;
}

/*
 * Insert a new thread in the ready list.
 */
void ready(tcb_t *tcb) {
	uint32_t flags;
	queue_thread_t *cur;

	spin_lock_irqsave(&schedule_lock, flags);

	tcb->state = THREAD_STATE_READY;

	cur = (queue_thread_t *) malloc(sizeof(queue_thread_t));
	BUG_ON(cur == NULL);

	cur->tcb = tcb;

	/* Insert the thread at the end of the list */
	list_add_tail(&cur->list, &readyThreads);

	spin_unlock_irqrestore(&schedule_lock, flags);
}

/*
 * Put a thread in the waiting state and invoke the scheduler.
 * It is assumed that the caller manages the waiting queue.
 */
void waiting(void) {
	uint32_t flags;

	flags = local_irq_save();

	ASSERT(current()->state == THREAD_STATE_RUNNING);

	current()->state = THREAD_STATE_WAITING;

	schedule();

	local_irq_restore(flags);
}

/*
 * Put a thread into the zombie queue.
 */
void zombie(void) {
	queue_thread_t *cur;
	uint32_t flags;

	ASSERT(current()->state == THREAD_STATE_RUNNING);

	flags = local_irq_save();

	current()->state = THREAD_STATE_ZOMBIE;

	cur = (queue_thread_t *) malloc(sizeof(queue_thread_t));
	BUG_ON(cur == NULL);

	cur->tcb = current();

	/* Insert the thread at the end of the list */
	list_add_tail(&cur->list, &zombieThreads);

	local_irq_restore(flags);

	schedule();
}

/*
 * Remove a thread from the zombie list.
 */
void remove_zombie(struct tcb *tcb) {
	queue_thread_t *cur;
	tcb_t *_tcb;
	struct list_head *pos, *q;

	ASSERT(local_irq_is_disabled());

	ASSERT(tcb != NULL);
	ASSERT(tcb->state == THREAD_STATE_ZOMBIE);

	list_for_each_safe(pos, q, &zombieThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);

		_tcb = cur->tcb;

		if (tcb == _tcb) {
			list_del(pos);

			free(cur);
			return ;
		}

	}

	printk("%s: zombie thread %d not found.\n", __func__, tcb->tid);

	BUG();
}

/*
 * Wake up a thread which is in waiting state.
 * If the thread passed as argument is not sleeping, we just call schedule().
 */
void wake_up(struct tcb *tcb) {

	ready(tcb);
	schedule();
}

/*
 * Remove a tcb from the ready list
 */
void remove_ready(struct tcb *tcb) {

	queue_thread_t *cur;
	tcb_t *_tcb;
	struct list_head *pos, *q;

	ASSERT(local_irq_is_disabled());
	ASSERT(tcb != NULL);

	ASSERT(tcb->state == THREAD_STATE_READY);

	spin_lock(&schedule_lock);

	list_for_each_safe(pos, q, &readyThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);
		_tcb = cur->tcb;

		if (tcb == _tcb) {
			list_del(pos);

			free(cur);

			spin_unlock(&schedule_lock);
			return ;
		}

	}

	printk("%s: ready thread %d not found.\n", __func__, tcb->tid);

	BUG();
}


/*
 * Pick up the next ready thread to be scheduled according
 * to the scheduling policy.
 * IRQs are off.
 */
static tcb_t *next_thread(void) {
	tcb_t *tcb;
	queue_thread_t *entry;

	ASSERT(local_irq_is_disabled());

	spin_lock(&schedule_lock);

	if (!list_empty(&readyThreads)) {
		entry = list_entry(readyThreads.next, queue_thread_t, list);

		spin_unlock(&schedule_lock);

		/* Warning ! entry will be freed in remove_ready() */
		tcb = entry->tcb;
		remove_ready(tcb);

		return tcb;
	}

	spin_unlock(&schedule_lock);

	return NULL;
}

extern u64 delta;

/*
 * Main scheduling function.
 */
void schedule(void) {

	tcb_t *prev, *next;
	uint32_t flags;

	flags = local_irq_save();

	/* Scheduling policy: at the moment start the first ready thread */
	
	prev = current();
	next = next_thread();

	if (unlikely((next == NULL) && ((prev == NULL) || (prev->state != THREAD_STATE_RUNNING))))
		next = tcb_idle;

	set_timer(&schedule_timer, NOW() + MILLISECS(SCHEDULE_FREQ));

	/* It may happen, at the very beginning, that the tcb_idle is not initialized yet */
	if ((next != NULL) && (next != prev)) {
		DBG("Now scheduling thread ID: %d name: %s\n", next->tid, next->name);

		/*
		 * The current threads (here prev) can be in different states, not only running; it may be in *waiting* or *zombie*
		 * depending on the thread activities. Hence, we put it in the ready state ONLY if the thread is in *running*.
		 */
		if ((prev != NULL) && (prev->state == THREAD_STATE_RUNNING) && (likely(prev != tcb_idle)))
			ready(prev);

		next->state = THREAD_STATE_RUNNING;
		set_current(next);

#ifdef CONFIG_MMU
		if ((next->pcb != NULL) && (next->pcb->pgtable != current_pgtable())) {
			mmu_switch(next->pcb->pgtable);
                        set_pgtable(next->pcb->pgtable);

                }
#endif /* CONFIG_MMU */

		/* Authorized to leave the interrupt context here */
		__in_interrupt = false;

		__switch_context(prev, next);

	}

	local_irq_restore(flags);
}

static inline void raise_schedule(void *__dummy) {
	raise_softirq(SCHEDULE_SOFTIRQ);
}

/*
 * Dump the ready threads
 */
void dump_ready(void) {
	struct list_head *pos;
	queue_thread_t *cur;

	printk("Dumping the ready-threads queue: \n");

	spin_lock(&schedule_lock);

	if (list_empty(&readyThreads)) {
		printk("  <empty>\n");
		return ;
	}

	list_for_each(pos, &readyThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);
		printk("  Thread ID: %d name: %s state: %d\n", cur->tcb->tid, cur->tcb->name, cur->tcb->state);

	}

	spin_unlock(&schedule_lock);
}

/*
 * Dump the zombie threads
 */
void dump_zombie(void) {
	struct list_head *pos;
	queue_thread_t *cur;

	printk("Dumping the zombie-threads queue: \n");

	if (list_empty(&zombieThreads)) {
		printk("  <empty>\n");
		return ;
	}

	list_for_each(pos, &zombieThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);
		printk("  Proc ID: %d Thread ID: %d state: %d\n", ((cur->tcb->pcb != NULL) ? cur->tcb->pcb->pid : 0), cur->tcb->tid, cur->tcb->state);

	}

}

/*
 * Dump information of the scheduling waitqueues.
 */
void dump_sched(void) {
	dump_ready();
	dump_zombie();
}

void scheduler_init(void) {

	/* Low-level thread initialization */
	threads_init();

	/* Initialize the main queues addressed by the scheduler */
	INIT_LIST_HEAD(&readyThreads);
	INIT_LIST_HEAD(&zombieThreads);

	/* Initialize the global list of processes */
	INIT_LIST_HEAD(&proc_list);

	spin_lock_init(&schedule_lock);

	/* Registering our softirq to activate the scheduler when necessary */
	register_softirq(SCHEDULE_SOFTIRQ, schedule);

	set_current(NULL);

	/* Initiate a timer to trigger the schedule function */
	init_timer(&schedule_timer, raise_schedule, NULL);

	set_timer(&schedule_timer, NOW() + MILLISECS(SCHEDULE_FREQ));
}
