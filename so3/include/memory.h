/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 * 
 * This software is released under the MIT License whose terms are defined hereafter.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef MEMORY_H
#define MEMORY_H

#include <types.h>
#include <list.h>

#include <asm/memory.h>

#include <generated/autoconf.h>

/* Transitional page used for temporary mapping */
#define TRANSITIONAL_MAPPING	0xf0000000

#define IO_MAPPING_BASE		0xe0000000

extern struct list_head io_maplist;

/* Manage the io_maplist. The list is sorted by ascending vaddr. */
typedef struct {
	uint32_t vaddr;	/* Virtual address of the mapped I/O range */
	uint32_t paddr; /* Physical address of this mapping */
	size_t size;	/* Size in bytes */

	struct list_head list;
} io_map_t;

/* PAGE_SHIFT determines the page size */
#define PAGE_SHIFT	12
#define PAGE_SIZE       (1 << PAGE_SHIFT)
#define PAGE_MASK       (~(PAGE_SIZE-1))

struct mem_info {
    uint32_t phys_base;
    uint32_t size;
    uint32_t avail_pages; /* Available pages including frame table, without the low kernel region */
};
typedef struct mem_info mem_info_t;

extern mem_info_t mem_info;

/*
 * Frame table which is constituted by the set of struct page.
 */
struct page {
	/* If the page is not mapped yet, and hence free. */
	bool free;
};
typedef struct page page_t;

extern page_t *frame_table;

#define pfn_to_phys(pfn) ((pfn) << PAGE_SHIFT)
#define phys_to_pfn(phys) (((uint32_t) phys) >> PAGE_SHIFT)
#define virt_to_pfn(virt) (phys_to_pfn(__va((uint32_t) virt)))

#define __pa(vaddr) (((uint32_t) vaddr) - CONFIG_KERNEL_VIRT_ADDR + ((uint32_t) CONFIG_RAM_BASE))
#define __va(paddr) (((uint32_t) paddr) - ((uint32_t) CONFIG_RAM_BASE) + CONFIG_KERNEL_VIRT_ADDR)

#define page_to_pfn(page) ((uint32_t) ((uint32_t) ((page)-frame_table) + phys_to_pfn(__pa((uint32_t) frame_table))))
#define pfn_to_page(pfn) (&frame_table[((pfn)-(__pa((uint32_t) frame_table) >> PAGE_SHIFT))])

#define page_to_phys(page) (pfn_to_phys(page_to_pfn(page)))
#define phys_to_page(phys) (pfn_to_page(phys_to_pfn(phys)))

void clear_bss(void);
void init_mmu(void);
void memory_init(void);

void frame_table_init(uint32_t frame_table_start);

/* Get memory informations from a device tree */
int get_mem_info(const void *fdt, mem_info_t *info);

void dump_frame_table(void);

uint32_t get_free_page(void);
void free_page(uint32_t paddr);

uint32_t get_free_vpage(void);
void free_vpage(uint32_t vaddr);

uint32_t get_contig_free_pages(uint32_t nrpages);
uint32_t get_contig_free_vpages(uint32_t nrpages);
void free_contig_pages(uint32_t page_phys, uint32_t nrpages);
void free_contig_vpages(uint32_t page_phys, uint32_t nrpages);

uint32_t get_kernel_size(void);

uint32_t *current_pgtable(void);
inline void set_pgtable(uint32_t *pgtable);

void init_io_mapping(void);
uint32_t io_map(uint32_t phys, size_t size);
void io_unmap(uint32_t vaddr);
io_map_t *find_io_map_by_paddr(uint32_t paddr);
void readjust_io_map(unsigned pfn_offset);

void dump_io_maplist(void);

#endif /* MEMORY_H */
