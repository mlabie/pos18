/*
 * pru_icss_intc.h
 *
 *  Created on: Jan 11, 2019
 *      Author: reds
 */
#include <types.h>

#ifndef INCLUDE_DEVICE_ARCH_PRU_ICSS_INTC_H_
#define INCLUDE_DEVICE_ARCH_PRU_ICSS_INTC_H_

#define GER		0x0010
#define SECR0	0x0280
#define SECR1	0x0284
#define SPIR0 	0x0D00
#define SPIR1 	0x0D04
#define SITR0 	0x0D80
#define SITR1 	0x0D84
#define HIER	0x1500

#define SET_TYPE_PULSE			0x0 /* set pulse */
#define CLEAR_SECR				0x1
#define POLARITY_31_0_HIGH		0x1	/* set high polarity */
#define POLARITY_63_32_HIGH		0x1 /* set high polarity */
#define EN_HINT					0x1 /* enable host interrupt */
#define EN_HINT_ANY 			0x1 /* Globally enable all interrupts */

#endif /* INCLUDE_DEVICE_ARCH_PRU_ICSS_INTC_H_ */
