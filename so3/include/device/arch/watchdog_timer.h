#ifndef WATCHDOG_TIMER_H
#define WATCHDOG_TIMER_H


#define SOFTRESET		(1 << 1)


#ifdef CONFIG_WDT
/**
 * Fonction effectuant un reset du watchdog.
 */
void reset_watchdog();
#endif


struct wdt {
	volatile u32 wdt_widr;				/* 0x00 */
	volatile u32 not_used1[3];			/* 0x04 */
	volatile u32 wdt_wdsc;				/* 0x10 */
	volatile u32 wdt_wdst;				/* 0x14 */
	volatile u32 wdt_wisr;				/* 0x18 */
	volatile u32 wdt_wier;				/* 0x1C */
	volatile u32 not_used2;				/* 0x20 */
	volatile u32 wdt_wclr;				/* 0x24 */
	volatile u32 wdt_wcrr;				/* 0x28 */
	volatile u32 wdt_wldr;				/* 0x2C */
	volatile u32 wdt_wtgr;				/* 0x30 */
	volatile u32 wdt_wwps;				/* 0x34 */
	volatile u32 not_used3[3];			/* 0x38 */
	volatile u32 wdt_wdly;				/* 0x44 */
	volatile u32 wdt_wspr;				/* 0x48 */
	volatile u32 wdt_wirqstatraw;		/* 0x54 */
	volatile u32 wdt_wirqstat;			/* 0x58 */
	volatile u32 wdt_wirqenset;			/* 0x5C */
	volatile u32 wdt_wirqenclr;			/* 0x60 */
};



#endif /* WATCHDOG_TIMER_H */
