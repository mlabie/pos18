/*
 * intc.h
 *
 *  Created on: Jan 9, 2019
 *      Author: reds
 */

#ifndef INCLUDE_DEVICE_ARCH_INTC_H_
#define INCLUDE_DEVICE_ARCH_INTC_H_

#include <types.h>

// page 193 du manuel TI
#define NR_IRQS          	128

#define REG_IRQ0			0x0
#define REG_IRQ1			0x1
#define REG_IRQ2			0x2
#define REG_IRQ3			0x3

#define NEWIRQAGR			0
#define NEWFIRQAGR			1

#define NOFUN_NO			(0 << 0)
#define NEWIRQ_RESET		(1 << 0)

#define SOFTRESET			(1 << 1)


struct intc_regs {

	volatile uint32_t intc_revision[4];		/* 0x0000 */
	volatile uint32_t intc_sysconfig; 		/* 0x0010 */
	volatile uint32_t intc_systatus[11];	/* 0x0014 */
	volatile uint32_t intc_sir_irq;			/* 0x0040 */
	volatile uint32_t intc_sir_fiq;			/* 0x0044 */
	volatile uint32_t intc_ctrl;			/* 0x0048 */
	volatile uint32_t intc_protection; 		/* 0x004C */
	volatile uint32_t intc_idle[4];			/* 0x0050 */
	volatile uint32_t intc_irq_priority;	/* 0x0060 */
	volatile uint32_t intc_fiq_priority;	/* 0x0064 */
	volatile uint32_t intc_threshold[6];	/* 0x0068 */

	volatile uint32_t intc_itr0;			/* 0x0080 */
	volatile uint32_t intc_mir0;			/* 0x0084 */
	volatile uint32_t intc_mir_clear0;		/* 0x0088 */
	volatile uint32_t intc_mir_set0;		/* 0x008C */
	volatile uint32_t intc_isr_set0;		/* 0x0090 */
	volatile uint32_t intc_isr_clear0;		/* 0x0094 */
	volatile uint32_t intc_pending_irq0;	/* 0x0098 */
	volatile uint32_t intc_pending_fiq0;	/* 0x009C */

	volatile uint32_t intc_itr1;			/* 0x00A0 */
	volatile uint32_t intc_mir1;			/* 0x00A4 */
	volatile uint32_t intc_mir_clear1;		/* 0x00A8 */
	volatile uint32_t intc_mir_set1;		/* 0x00AC */
	volatile uint32_t intc_isr_set1;		/* 0x00B0 */
	volatile uint32_t intc_isr_clear1;		/* 0x00B4 */
	volatile uint32_t intc_pending_irq1;	/* 0x00B8 */
	volatile uint32_t intc_pending_fiq1;	/* 0x00BC */

	volatile uint32_t intc_itr2;			/* 0x00C0 */
	volatile uint32_t intc_mir2;			/* 0x00C4 */
	volatile uint32_t intc_mir_clear2;		/* 0x00C8 */
	volatile uint32_t intc_mir_set2;		/* 0x00CC */
	volatile uint32_t intc_isr_set2;		/* 0x00D0 */
	volatile uint32_t intc_isr_clear2;		/* 0x00D4 */
	volatile uint32_t intc_pending_irq2;	/* 0x00D8 */
	volatile uint32_t intc_pending_fiq2;	/* 0x00DC */

	volatile uint32_t intc_itr3;			/* 0x00E0 */
	volatile uint32_t intc_mir3;			/* 0x00E4 */
	volatile uint32_t intc_mir_clear3;		/* 0x00E8 */
	volatile uint32_t intc_mir_set3;		/* 0x00EC */
	volatile uint32_t intc_isr_set3;		/* 0x00F0 */
	volatile uint32_t intc_isr_clear3;		/* 0x00F4 */
	volatile uint32_t intc_pending_irq3;	/* 0x00F8 */
	volatile uint32_t intc_pending_fiq3;	/* 0x00FC */
	volatile uint32_t intc_ilr[NR_IRQS];	/* 0x0100 */

};

#endif /* INCLUDE_DEVICE_ARCH_INTC_H_ */
