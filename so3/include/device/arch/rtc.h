/*
 * rtc.h
 *
 *  Created on: Jan 20, 2019
 *      Author: reds
 */

#ifndef INCLUDE_DEVICE_ARCH_RTC_H_
#define INCLUDE_DEVICE_ARCH_RTC_H_

#include <types.h>

#define RUN_RTC	(1 << 0)
#define ENABLE_INTERRUPT_ALARM2	(1 << 3)
#define ENABLE_INTERRUPT_ALARM1	(1 << 2)
#define ENABLE_INTERRUPT_TIMER	(1 << 2)
#define INTERRUPT_EVERY_SECOND 	(0 << 0)
#define INTERRUPT_EVERY_MINUTE 	(1 << 0)
#define INTERRUPT_EVERY_HOUR 	(2 << 0)
#define INTERRUPT_EVERY_DAY		(3 << 0)
#define NO_IDLE_MODE 			(1 << 0)



/**
 * Génère une chiffr aléatoire à partir de la
 * Real Time Clock
 *
 * @length : Nombre de digit du chiffre aléatoire
 */
u32 random_number(u32 length);


struct rtc {

	volatile u32 seconds_reg;			/* 0x00 */
	volatile u32 minutes_reg;			/* 0x04 */
	volatile u32 hours_reg;				/* 0x08 */
	volatile u32 days_reg;				/* 0x0C */
	volatile u32 months_reg;			/* 0x10 */
	volatile u32 years_reg;				/* 0x14 */
	volatile u32 weeks_reg[2];			/* 0x18 */
	volatile u32 alarm_seconds_reg;		/* 0x20 */
	volatile u32 alarm_minutes_reg;		/* 0x24 */
	volatile u32 alarm_hours_reg;		/* 0x28 */
	volatile u32 alarm_days_reg;		/* 0x2C */
	volatile u32 alarm_months_reg;		/* 0x30 */
	volatile u32 alarm_years_reg[3];	/* 0x34 */
	volatile u32 rtc_ctrl_reg;			/* 0x40 */
	volatile u32 rtc_status_reg;		/* 0x44 */
	volatile u32 rtc_interrupts_reg;	/* 0x48 */
	volatile u32 rtc_comp_lsb_reg;		/* 0x4C */
	volatile u32 rtc_comp_msb_reg;		/* 0x50 */
	volatile u32 rtc_osc_reg[3];		/* 0x54 */
	volatile u32 rtc_scratch0_reg;		/* 0x60 */
	volatile u32 rtc_scratch1_reg;		/* 0x64 */
	volatile u32 rtc_scratch2_reg;		/* 0x68 */
	volatile u32 kick0r;				/* 0x6C */
	volatile u32 kick1r;				/* 0x70 */
	volatile u32 rtc_revision;			/* 0x74 */
	volatile u32 rtc_sysconfig;			/* 0x78 */
	volatile u32 rtc_irqwakeen;			/* 0x7C */
	volatile u32 alarm2_seconds_reg;	/* 0x80 */
	volatile u32 alarm2_minutes_reg;	/* 0x84 */
	volatile u32 alarm2_hours_reg;		/* 0x88 */
	volatile u32 alarm2_days_reg;		/* 0x8C */
	volatile u32 alarm2_months_reg;		/* 0x90 */
	volatile u32 alarm2_years_reg;		/* 0x94 */
	volatile u32 rtc_pmic;				/* 0x98 */
	volatile u32 rtc_debounce;			/* 0x9C */
};

#endif /* INCLUDE_DEVICE_ARCH_RTC_H_ */
