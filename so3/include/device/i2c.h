/*
 * so3_i2c.h
 */

#ifndef INCLUDE_SO3_I2C_H_
#define INCLUDE_SO3_I2C_H_

#include <common.h>


int i2c_init(int requested_speed, int slaveadd);
int i2c_read(char dev, int addr, int alen, char *data, int length);
int i2c_write(char dev, int addr, int alen, char *data, int length);

#endif /* INCLUDE_SO3_I2C_H_ */
