/* so3_timer.h */

#ifndef GPIO_H_
#define GPIO_H_

#include <common.h>   /* struct so3_timer_ops */

extern struct so3_gpio_ops *ops_gpio;

int gpio_config_dir(int num, int output);

int gpio_get(int num);

int gpio_set(int num, int status);

int gpio_interrupt(int num, void (*callback)(void));

#endif /* GPIO_H_ */
