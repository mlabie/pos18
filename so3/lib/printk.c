/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 *
 * This software is released under the MIT License whose terms are defined hereafter.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - 2017-2018: Daniel Rossier
 *
 */

#include <string.h>
#include <common.h>
#include <stdarg.h>

#include <device/serial.h>

#ifdef CONFIG_SO3VIRT
#include <virtshare/dev/vuart.h>
#endif

void printk(const char *fmt, ...)
{
	static char   buf[1024];

	va_list       args;
	char         *p, *q;

	va_start(args, fmt);
	(void)vsnprintf(buf, sizeof(buf), fmt, args);
	va_end(args);

	p = buf;

	while ((q = strchr(p, '\n')) != NULL)
	{
		*q = '\0';

#if defined(CONFIG_SO3VIRT) && defined(CONFIG_VUART_FRONTEND)
		vuart_write(p, strlen(p)+1);
		vuart_write("\n", 2);
#else
		serial_write(p, strlen(p)+1);
		serial_write("\n", 2);
#endif /* CONFIG_SO3VIRT && CONFIG_VUART_FRONTEND */

		p = q + 1;
	}

	if (*p != '\0') {
#if defined(CONFIG_SO3VIRT) && defined(CONFIG_VUART_FRONTEND)
		vuart_write(p, strlen(p)+1);
#else
		serial_write(p, strlen(p)+1);
#endif /* CONFIG_SO3VIRT && CONFIG_VUART_FRONTEND */
	}

}

