/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 *
 * This software is released under the MIT License whose terms are defined hereafter.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - 2016 - 2018: Daniel Rossier
 *
 */


#include <common.h>
#include <softirq.h>
#include <thread.h>
#include <heap.h>
#include <string.h>
#include <thread.h>

#include <device/irq.h>

static irqdesc_t irqdesc[NR_IRQS];
bool __in_interrupt = false;

irq_ops_t irq_ops;

irqdesc_t *irq_to_desc(uint32_t irq) {
	return &irqdesc[irq];
}

/*
 * Main thread entry point for deferred processing.
 * At the entry, IRQs are on.
 */
int __irq_deferred_fn(void *args) {
	int ret;
	uint32_t irq = *((uint32_t *) args);

	while (irqdesc[irq].deferred_pending) {


		irqdesc[irq].deferred_pending = false;

		local_irq_enable();

		/* Perform the deferred processing bound to this IRQ */
		ret = irqdesc[irq].irq_deferred_fn(irq, irqdesc[irq].data);

		local_irq_disable();


		/* At this point, we are coherent; if the same IRQ occurred before the end of this thread,
		 * the deferred_pending boolean is passed to true, and the while{} will be re-executed once more.
		 * If deferred_pending remains to false, and since IRQs are now disabled, the top half processing
		 * has not reached the assignment of deferred_pending to true, and therefore a new thread will
		 * be re-spawned. We are safe.
		 */
	}

	irqdesc[irq].thread_active = false;

	/* Re-enabling IRQs */
	local_irq_enable();

	/* Release the heap memory allocated for args */
	free(args);

	return ret;
}

/*
 * Process interrupt with top & bottom halves processing.
 */
int irq_process(uint32_t irq) {
	int ret;
	char th_name[THREAD_NAME_LEN];
	int *args;

	/* Immediate (top half) processing */
	if (irqdesc[irq].action != NULL)
		ret = irqdesc[irq].action(irq, irqdesc[irq].data);

	/*
	 * Deferred (bottom half) processing.
	 * A thread is created and started if it is the case.
	 */
	ASSERT(local_irq_is_disabled());

	if ((ret == IRQ_BOTTOM) && (irqdesc[irq].irq_deferred_fn != NULL)) {

		irqdesc[irq].deferred_pending = true;

		if (!irqdesc[irq].thread_active) {

			irqdesc[irq].thread_active = true;

			args = malloc(sizeof(uint32_t));
			BUG_ON(!args);

			memcpy(args, &irq, sizeof(uint32_t));

			sprintf(th_name, "irq_bottom/%d", irq);

			kernel_thread(__irq_deferred_fn, th_name, args);
		}
	}

	return ret;
}

/*
 * Bind a IRQ number with a specific top half handler and bottom half if any.
 */
void irq_bind(int irq, irq_handler_t handler, irq_handler_t irq_deferred_fn, void *data) {

	DBG("Binding irq %d with action at %x\n", irq, handler);

	BUG_ON(irqdesc[irq].action != NULL);

	irqdesc[irq].action = handler;
	irqdesc[irq].irq_deferred_fn = irq_deferred_fn;
	irqdesc[irq].data = data;

	irq_ops.irq_enable(irq);
}

void irq_unbind(int irq) {

//	DBG("Binding irq %d with action at %x\n", irq, handler);
	irqdesc[irq].action = NULL;
	irqdesc[irq].irq_deferred_fn = NULL;
}

void irq_mask(int irq) {

	irq_ops.irq_mask(irq);
}

void irq_unmask(int irq) {

	irq_ops.irq_unmask(irq);
}

void irq_enable(int irq) {

	irq_ops.irq_enable(irq);
	irq_ops.irq_unmask(irq);
}

void irq_disable(int irq) {

	irq_ops.irq_mask(irq);
	irq_ops.irq_disable(irq);
}

void irq_handle(void) {

	/* The following boolean indicates we are currently in the interrupt call path.
	 * It will be reset at the end of the softirq processing.
	 */

	__in_interrupt = true;

	irq_ops.irq_handle();

}

/*
 * Main interrupt device initialization function
 */
void irq_init(void) {
	int i;

	memset(&irq_ops, 0, sizeof(irq_ops_t));


	for (i = 0; i < NR_IRQS; i++) {
		irqdesc[i].action = NULL;
		irqdesc[i].irq_deferred_fn = NULL;
		irqdesc[i].deferred_pending = false;
		irqdesc[i].thread_active = false;
	}

	/* Initialize the softirq subsystem */
	softirq_init();
}
