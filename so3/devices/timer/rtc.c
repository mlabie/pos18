/*
 * rtc_timer.c
 *
 *  Created on: Jan 20, 2019
 *      Author: reds
 */


#include <types.h>
#include <heap.h>
#include <device/arch/rtc.h>
#include <device/device.h>
#include <device/driver.h>
#include <device/irq.h>				// Pour traitement d'interruption

#define MASK_UNITE		0x0F
#define MASK_DIZAINE 	0x70

#define MASK_DIZAINE_HEURE 	0x30


static dev_t rtc_dev;
static struct rtc* rtc;

//===================================================================//
//========== 		 	RANDOM FUNCTION	   		   ==========//
//===================================================================//

u32 number_of_digit(u32 val){
	u32 count = 0;
	do{
		count++;
		val /= 10;
	}while(val != 0);

	return count;
}


u32 random_number(u32 length){
	u32 random_num;

	char tmp[length];
	int count = length;

	u32 sec_u, sec_d; 		//unites et dizaine des secondes
	u32 min_u, min_d; 		//unites et dizaine des minutes
	u32 hours_u, hours_d;	//unites et dizaine des heures
	u32 time;
	u32 pow;
	u32 div;

	u32 nbr_digit;
	uint64_t seed;


	u32 k = 2;
	int i;

	sec_u = rtc->seconds_reg & MASK_UNITE;
	sec_d = (rtc->seconds_reg & MASK_DIZAINE) >> 4;

	min_u = rtc->minutes_reg & MASK_UNITE;
	min_d = (rtc->minutes_reg & MASK_DIZAINE) >> 4;

	hours_u = rtc->hours_reg & MASK_UNITE;
	hours_d = (rtc->hours_reg & MASK_DIZAINE_HEURE) >> 4;


	random_num = 0;

	time = (uint64_t)((sec_u + sec_d*10) + ((min_u + min_d*10) * 60) + ((hours_u + hours_d*10) * 3600));


	do{
		time = time * time;

		nbr_digit = number_of_digit(time);


		if(nbr_digit <= k){
			seed = time;
		}else{
			pow = ((nbr_digit - k)/2);
			div = 1;
			while(pow--){
				div *= 10;
			}

			seed = time / div;
		}



		for(i = k; i > 0; i--){
			if(count-i < 0)
				continue;

			tmp[count-i] = seed % 10;
			//printk("tmpval : %d\n", tmp[count-i]);
			seed /= 10;
		}


		count -= k;

	}while(count > 0);



	for(i = length-1; i >= 0; i--){
		random_num *= 10;
		random_num += tmp[i];
	}

	return random_num;
}



//===================================================================//
//========== 		 	RTC	   		   ==========//
//===================================================================//


static irq_return_t rtc_irq_handler(int irq, void *data){
	irq_return_t irq_ret;

	int sec_u, sec_d; 		//unites et dizaine des secondes
	int min_u, min_d; 		//unites et dizaine des minutes
	int hours_u, hours_d;	//unites et dizaine des heures

	sec_u = rtc->seconds_reg & MASK_UNITE;
	sec_d = (rtc->seconds_reg & MASK_DIZAINE) >> 4;

	min_u = rtc->minutes_reg & MASK_UNITE;
	min_d = (rtc->minutes_reg & MASK_DIZAINE) >> 4;

	hours_u = rtc->hours_reg & MASK_UNITE;
	hours_d = (rtc->hours_reg & MASK_DIZAINE_HEURE) >> 4;

	//affichage du temps passé depuis l'activation du timer
	printk("RTC HH:MM:SS %d%d:%d%d:%d%d\n", hours_d, hours_u, min_d, min_u, sec_d, sec_u);
	printk("RANDOM : %d\n", random_number(8));

	// On doit retourner IRQ_BOTTOM pour que l'action différée soit effectuée à son tour.
	irq_ret = IRQ_COMPLETED;

	return irq_ret;
}

static int rtc_init(dev_t *dev) {

	//NOTE: Par défault la RTC générera une interruption toute les secondes

	memcpy(&rtc_dev, dev, sizeof(dev_t));
	rtc = (struct dm_timer*)(rtc_dev.base);

	//activation des interruption pour les timers
	rtc->rtc_interrupts_reg = ENABLE_INTERRUPT_TIMER;

	//run la RTC
	rtc->rtc_ctrl_reg = RUN_RTC;

	rtc->rtc_sysconfig = NO_IDLE_MODE;

	// on lie la fonction de gestion de l'interruption a notre device
	irq_bind(rtc_dev.irq, &rtc_irq_handler, NULL, NULL);

	return 0;
}


REGISTER_DRIVER(rtc, "rtcss", rtc_init);
