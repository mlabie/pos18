# BeagleBone by BeagleBoys

Christophe Joyet & Marc Labie

## Etape U-Boot - Board BeagleBone

### Partie 1: Commande "Hello World"

Il faut dans un premier temps aller dans le répertoire /cmd de u-boot, où sont stockées toutes les commandes. 
Ici, nous avons créé un nouveau fichier hello.c, qui contient le code de notre commande "hello".

Dans le répertoire /u-boot/doc, nous trouvons un fichier README.commands.
Celui-ci nous explique comment ajouter une nouvelle commande à u-boot.
Il faut créer un nouvelle structure de commande, définit comme suit :

```c
U_BOOT_CMD(name, maxargs, repeatable, command, "usage", "help")
```
Dans un premier temps, il faut ajouter l'include "command.h" :

```c
/**
* hello.c
*/

#include <command.h>
```

Nous ajoutons ensuite la stucture demandée :

```c
/**
* hello.c
*/

#include <command.h>



U_BOOT_CMD(hello, 1, 0, do_hello, "prints \"Hello World!\" in the console", "prints \"Hello World!\" in the console");

```

Il nous faut donc à présent ajouter la fonction `do_hello` qui exectuera le code lors de l'appelle de la commande hello :


```c
/**
* hello.c
*/

#include <command.h>
```

Nous ajoutons ensuite la stucture demandée :

```c
/**
* hello.c
*/

#include <command.h>
#include <stdio.h>

static int do_hello(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]){
	printf("Hello World!\n");
}


U_BOOT_CMD(hello, 1, 0, do_hello, "prints \"Hello World!\" in the console", "prints \"Hello World!\" in the console");

```

Notre commande est à présent prête. 
Il faut cependant encore ajouter notre fichier hello.c dans le makefile pour qu'il le compile :

```Makefile
...
obj-$(CONFIG_CMD_HELLO) += hello.o
...
```

Maintenant, il faut ajouter au fichier Kconfig de /cmd notre nouvelle commande, 
pour qu'un future utilisateur (nous) puisse définir s'il souhaite ajouter la commande "hello" à son u-boot compiler ou non :

```
...

config CMD_HELLO
	bool "hello"
	default y
	help
	  Print "Hello World!" to console
	  
...

```

Nous avons mis le default à y, pour dire que, par défaut, hello sera ajouté à u-boot. 
Il n'est donc pas nécessaire d'éditer la configuration ici.
Il nous reste donc plus qu'à compiler u-boot, à le lancer sur qemu, et le tour est joué !

### Partie 2: Déploiement de U-boot

La Beaglebone black embarque un processeur AM335x de Texas Instruments. 
Cela implique, tout comme pour la Reptar, d'utiliser un premier bootloader avant que celui-ci n'appelle u-boot.

#### Partie 2.1 : Partitionnage de la carte SD

Afin de partitionner la carte SD, nous avons suivi les étapes 1 à 3b du site suivant : 
http://www.armhf.com/boards/beaglebone-black/bbb-sd-install/

Il crée ici déjà deux partition, en vue de pouvoir placer linux sur la seconde. Seulement la première nous intéresse.

#### Partie 2.2 : Configuration de u-boot

La configuration d'u-boot pour la BeagleBone est am335x_evm. 

source : http://beagleboard.org/project/U-Boot+%28V1%29/

Afin d'effectuer la configuration de u-boot, lancez ces commandes : 
```
...
make am335x_evm_defconfig
make -j8
...
```

Prendre les fichiers MLO et u-boot.img et les placer dans dans la partition de boot de la carte sd.
```
...
sudo mount /dev/sdX1 fs
cp u-boot.img fs
cp MLO fs
sudo umount fs
...
```
Branchement de l'uart sur la BeagleBone Black
![Fig. 1 - Branchement de l'uart sur la BeagleBone Black](http://box.matto.nl/ftx/bbb-serial.jpg)


Pourquoi .img ?

Suivant les SoC, le boot ROM peut charger et exécuter directement le fichier .bin 
(comme c'est le cas pour la carte REPTAR).
Dans le cas de la BeagleBone Black, le bootROM a besoin d'un header qui 
donne des informations sur comment et où exécuter le fichier u-boot.bin.

Le fichier .img contient donc l'exécutable u-boot.bin ainsi que ce header. 

Pour plus d'informations : 
```
https://stackoverflow.com/questions/29494321/what-is-diffrent-between-u-boot-bin-and-u-boot-img
```
Pourquoi MLO ? 

Lors du démarrage de la carte, le bootROM va copier le contenu du bootloader dans la RAM. 
Afin d'optimiser la copie et donc le démarrage, on utilise ce pré-boot. <- à verifier avec M.Rossier

Pour plus d'informations : 
```
https://stackoverflow.com/questions/31244862/what-is-the-use-of-spl-secondary-program-loader
```

Séquence de démarrage schématisée : 
![Fig. 2 - Séquence de démarrage shcématisée](http://processors.wiki.ti.com/images/b/b6/BOOT_ORDER.png)


### Partie 3: Accès aux GPIO

Nous trouvons sur le lien suivant une carte interactive de la beaglebone nous permettant de savoir 
pour chaque GPIO à quelle banque de GPIO et à quel index il correspond :

```
https://troy.dack.com.au/static/GPIO_Map/
```

Nous voyons par exemple la led USR_0, est associée à la banque 1, à l'index 21.

Donc pour pouvoir controller la led, il va falloir écrire dans les différents registres de la banque 1 à l'index 21.
Pour cela, nous nous référons au manuel du cpu de la beaglonebone black, à savoi l'AM335X, que l'on peut trouver ici :

```
https://www.ti.com/lit/ug/spruh73p/spruh73p.pdf
```

Nous voyons à la page 180 que pour la banque de GPIO 1, la plage d'adresse des registres se situe entre 0x4804_C000 et 0x4804_CFFF.
A la page 4987, nous trouvons les offsets correspondant à chaque registre pour chacune des banque de GPIO, notamment l'offset 0x134, 
correspondant au registre GPIO_OE, l'offset 0x190, correspondant au registre GPIO_CLEARDATAOUT et l'offset 0x194, correspondant au registre GPIO_SETDATAOUT.

Nous avons donc créé une nouvelle commande "gpios" (fichier gpios.c dans le dossier cmd de u-boot) en utilisant la même démarche que celle de la partie 1 
(gpio étant déjà utilisée).

elle s'utilise comme suit :

```bash
$ gpios <bank number> [r | w] <offset> <value to write>
```

où : 
 - <bank number> correspond à la banque de GPIO où nous souhaitons lire/écrire
 - [r|w] définit l'action à effectuer (r = lire, w = écrire)
 - <offset> détermine le registre dans lequel nous souhaite lire/écrire en hexdécimal
 - <value to write> est la valeur que nous voulons écrire en hexdécimal (à mentionner seulement dans le cas de w)
 
Il est important de noter que, étant donné que u-boot se veut très léger et que donc, il n'intègre pas la librairire "string.h", nous avons limité 
la userfrendlyness de notre code. 
C'est à dire que pour la valeur hexadécimal de l'offset et de la valeur à écrire, celles-ci ne doivent pas commencer par 0x 
et seront toujours interprétées comme étant en hexadécimal.
De plus, nous n'effectuons pas tous les conrôles nécessaires, donc, veuillez s'il vous plaît respecter la synthaxe ci-dessus.

Dans le cas où nous voudrions par exemple allumer la led USR_0 de la beaglebone, voilà ce que nous devrions entrer :

```bash
$ gpios 1 w 134 ffdfffff
$ gpios 1 w 194 00200000
```

La première des deux commande va écrire la valeur 0xFFDFFFFF dans le registre GPIO_OE de banque 1, configurant la pin 21 (correspondant à la led) 
comme étant un output.
La seconde va allumer la led en mettant la même pin 21 à 1 (Avec 0x00200000) dans le registre GPIO_SETDATAOUT.

Pour éteindre la led, il faudra activer le bit 21 du registre GPIO_CLEARDATAOUT (offset 190) qui désactivera le bit mis à 1 du registre 
GPIO_SETDATAOUT.
