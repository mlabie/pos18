/*
 * hello.c
 *
 *  Created on: Oct 3, 2018
 *      Author: Marc Labie & Christophe Joyet
 */



#include <command.h>
#include <stdio.h>


static int do_hello(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]){
	printf("Hello World!\n");
}

U_BOOT_CMD(hello, 1, 0, do_hello, "prints \"Hello World!\" in the console", "prints \"Hello World!\" in the console");

